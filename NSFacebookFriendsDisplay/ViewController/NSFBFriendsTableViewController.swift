//
//  NSFBFriendsTableViewController.swift
//  NSFacebookFriendsDisplay
//
//  Created by Shenll_IMac on 17/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import UIKit
import CoreData

class NSFBFriendsTableViewController: UITableViewController {
    
    var fbFriendsListingFromCoreData = [Friends]()
    lazy var coreDataManager = CoreDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        if (self.fbFriendsListingFromCoreData.count > 0)
        {
            self.tableView.separatorStyle = .singleLine
            numOfSections                = 1
            self.tableView.backgroundView = nil
        }
        else
        {
            //let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            let noDataLabel = UILabel(frame: CGRect(x: 0,y :0, width:self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            
            noDataLabel.text              = "No data available. Login with Facebook to retrieve taggable friends"
            noDataLabel.numberOfLines     = 3
            noDataLabel.textColor         = UIColor.black
            noDataLabel.textAlignment     = .center
            self.tableView.backgroundView = noDataLabel
            self.tableView.separatorStyle = .none
        }
        
        return numOfSections
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.fbFriendsListingFromCoreData.count) > 0 {
            return self.fbFriendsListingFromCoreData.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        let friend = self.fbFriendsListingFromCoreData[indexPath.row] as Friends
        
        cell.textLabel?.text = friend.name
        // cell.detailTextLabel?.text = friend.id
        
        cell.imageView?.image = UIImage(named: "applogo")
        let iconUrl = friend.imageUrl
        cell.imageView?.downloadImageFrom(link: iconUrl!, contentMode: UIViewContentMode.scaleAspectFit)
        
        return cell
    }
    
    // MARK: - Custom Methods

    func getFBFriendsList(){
        
        self.fbFriendsListingFromCoreData.removeAll()
        
        let managedObjectContext = coreDataManager.managedObjectContext
        let entityFriend = NSEntityDescription.entity(forEntityName: "Friends", in: managedObjectContext)
        let request: NSFetchRequest<Friends> = Friends.fetchRequest()
        request.entity = entityFriend
        
        do {
            let fetchResults = try managedObjectContext.fetch(request)
            for entity in fetchResults {
                self.fbFriendsListingFromCoreData.append(entity)
            }
            print("Friends From Core Data : \(self.fbFriendsListingFromCoreData)")
        } catch {
            print(error)
            let saveError = error as NSError
            print(saveError)
        }
        
    }
    
    public func refreshTableView(){
        getFBFriendsList()
        self.tableView.reloadData()
    }
    
    public func refreshLogoutTableView(){
        self.fbFriendsListingFromCoreData.removeAll()
        self.tableView.reloadData()
    }
    
}

// MARK: - Image Loading Extension
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
