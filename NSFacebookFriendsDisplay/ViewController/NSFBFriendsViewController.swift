//
//  ViewController.swift
//  NSFacebookFriendsDisplay
//
//  Created by Shenll_IMac on 17/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

import CoreData

class NSFBFriendsViewController: UIViewController,LoginButtonDelegate {

    @IBOutlet weak var viewFabookButtonView: UIView!
    @IBOutlet weak var refreshFBFriendsButton: UIBarButtonItem!

    lazy var coreDataManager = CoreDataManager()
    
    override func  viewDidLoad() {
        
        configFacebookButton()
        
        if AccessToken.current != nil {
            // User is logged in, use 'accessToken' here.
            print("User logged in and access token is \(AccessToken.current)")
            refreshFBFriendsButton.isEnabled = true
            getFBFriends()
            
        }
        else{
            refreshFBFriendsButton.isEnabled = false
        }
    }

    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFriends" {
            //let controller = segue.destination as! NSFBFriendsTableViewController
            print("navigating to NSFBFriendsTableViewController..")
        }
    }
    
    // MARK: - Memory Warning
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Action
    @IBAction func refreshFBFriendsButtonClicked() {
        getFBFriends()
    }
    
    // MARK: - FB Login delegate methods
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
        switch result {
            case .failed(let error):
                print("Error: \(error)")
                showErrorAlert(errorMessage: "FB Login error : \(error)")
            case .cancelled:
                print("User cancelled login.")
                showErrorAlert(errorMessage: "User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("User Logged in and Login result is : \(result)")
                print("\(grantedPermissions)")
                print("\(declinedPermissions)")
                print("\(accessToken)")
                refreshFBFriendsButton.isEnabled = true
                getFBFriends()
            
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("User Logged out!")
        refreshFBFriendsButton.isEnabled = false
        self.refreshLogoutTableViewController()
    }
    
    // MARK: - Custome methods
    
    func configFacebookButton(){
        let loginButton = LoginButton(readPermissions: [ .publicProfile, .email, .userFriends ])
        loginButton.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 180, height: 40))
        viewFabookButtonView.addSubview(loginButton)
        loginButton.delegate = self;
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: viewFabookButtonView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: viewFabookButtonView, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        viewFabookButtonView.addConstraints([horizontalConstraint,verticalConstraint])
    }
    
    func getFBFriends(){
        
        let request = GraphRequest(graphPath:"/me/taggable_friends", parameters: [:]);
        
        request.start {
            (urlResponse, requestResult) in
            
            switch requestResult {
            case .failed(let error):
                print("error in graph request:", error)
                self.showErrorAlert(errorMessage: " FB Graph Request : \(error)")
                break
            case .success(let graphResponse):
                print("success")
                if let responseDictionary = graphResponse.dictionaryValue {
                    print(responseDictionary)
                    let data : NSArray = responseDictionary["data"] as! NSArray
                    
                    for friendCount in 0..<data.count {
                        print(friendCount)
                        let valueDict : NSDictionary = data[friendCount] as! NSDictionary
                        let fbFriendId = valueDict["id"] as! String
                        let fbFriendname = valueDict["name"] as! String
                        let pictureDict = valueDict["picture"] as! NSDictionary
                        let pictureData = pictureDict["data"] as! NSDictionary
                        let fbFriendImageUrl = pictureData["url"] as! String
                        
                        if(self.checkFriendAlreadyExistsInDB(id: fbFriendId)){
                            self.updateFriend(id : fbFriendId,
                                            name: fbFriendname,
                                            imageUrl : fbFriendImageUrl)
                        }
                        else{
                            self.saveFriend(id : fbFriendId,
                                            name: fbFriendname,
                                            imageUrl : fbFriendImageUrl)
                        }
                        
                    }
                    
                    self.refreshFriendsTableViewController()
                }
            }
        }
      
    }
    
    func saveFriend(id : String, name : String, imageUrl : String){
        let managedObjectContext = coreDataManager.managedObjectContext
        
        let entityFriend = NSEntityDescription.entity(forEntityName: "Friends", in: managedObjectContext)
        
        let friend = Friends(entity: entityFriend!,
                               insertInto: managedObjectContext)
        
        friend.id = id
        friend.name = name
        friend.imageUrl = imageUrl
        
        do {
            managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            try managedObjectContext.save()
        } catch let error {
            print("\(error.localizedDescription)")
            self.showErrorAlert(errorMessage: "Core Data Error : \(error.localizedDescription)")
        }
    }
    
    func updateFriend(id : String, name : String, imageUrl : String){
        
        let managedObjectContext = coreDataManager.managedObjectContext
        
        let fetchRequest: NSFetchRequest<Friends> = NSFetchRequest(entityName: "Friends")
        let predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext.fetch(fetchRequest)
            
            if fetchResults.count > 0 {
                let friend = fetchResults[0]
                friend.setValue(id, forKey: "id")
                friend.setValue(name, forKey: "name")
                friend.setValue(imageUrl, forKey: "imageUrl")
                try managedObjectContext.save()
            }
            else {
                // 
                print("no record to update")
            }
                
        } catch {
            let saveError = error as NSError
            print(saveError)
        }

    }
    
    func checkFriendAlreadyExistsInDB(id : String) -> Bool{
        
        let managedObjectContext = coreDataManager.managedObjectContext
        
        let fetchRequest: NSFetchRequest<Friends> = NSFetchRequest(entityName: "Friends")
        //let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Friends")
        let predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.predicate = predicate
        
        do {
            //var tempFriendsList = [Friends]()
            let fetchResults = try managedObjectContext.fetch(fetchRequest)
            //tempFriendsList = fetchResults as [Friends]
            
            if fetchResults.count > 0 {
                return true
            }
            else {
                // 
                print("no record to check")
            }
            
        } catch {
            let saveError = error as NSError
            print(saveError)
        }
        
        return false
    }
    
    func refreshFriendsTableViewController(){
        let childViewControllers = self.childViewControllers as NSArray
        let controller =  childViewControllers[0] as! NSFBFriendsTableViewController
        controller.refreshTableView()
    }
    
    func refreshLogoutTableViewController(){
        let childViewControllers = self.childViewControllers as NSArray
        let controller =  childViewControllers[0] as! NSFBFriendsTableViewController
        controller.refreshLogoutTableView()
    }
}

// MARK: - Alet Extension

extension NSFBFriendsViewController {
    func showErrorAlert(errorMessage: String) {
        let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
